app1.controller("Definitions", [ "$http",'$scope', "common", function($http, $scope, common) {
    var ctrl = this;
    ctrl.session = common.session;
    ctrl.definitions = {};

    ctrl.defuserData = {name:"", recipient_email: "", amount:0, description:""};

    ctrl.handleClick = ()=>{
        $("#newdefinition").slideToggle( "slow");
        console.log('call came here!');
      };

    ctrl.refreshDefinitions = function() {
        $http.get('/definitions').then(
            function(rep) { ctrl.definitions = rep.data;
              common.shareData = [...ctrl.definitions];
            },
            function(err) { ctrl.message = 'Error retrieving definitions!'; }
        );

    };
    ctrl.getOneById = function(_id) {
      console.log(_id);
        $http.get('/definitions?_id='+_id).then(
            function(rep) {
              ctrl.defuserData._id = rep.data[0]._id;
              ctrl.defuserData.name = rep.data[0].name;
              ctrl.defuserData.recipient_email =rep.data[0].recipient_email;
              ctrl.defuserData.amount = rep.data[0].amount;
              ctrl.defuserData.description = rep.data[0].description;
              console.log(ctrl.defuserData);
            },
            function(err) { ctrl.message = 'Error retrieving definition!'; }
          );
        $("#newdefinition").slideDown( "slow");
    };

      ctrl.zeroData = function() {
        ctrl.defuserData = {name:"", recipient_email: "", amount:0, description:""};
        ctrl.message = {};
        ctrl.handleClick();
    }
    ctrl.addDefinitions = function() {
        $http.post('/definitions', ctrl.defuserData).then(
            function(rep) {
                ctrl.refreshDefinitions();
                ctrl.zeroData();
                ctrl.message = {
                    ok: 'Definition saved! '
                };
            },
            function(err) {
                ctrl.message = { error: err.data.err };
            }
        );
    };
    ctrl.updateDefinition = function(_id)
    {
      console.log('here..',_id);
      $http.put('/definitions', ctrl.defuserData).then(
        function(rep){
          ctrl.refreshDefinitions();
          ctrl.zeroData();
          ctrl.message = {
              ok: 'Definition updated! '
          };
        },
        function(err) {
            ctrl.message = { error: err.data.err };
        }
      );
    };
    ctrl.definitionInvalid = function() {
        return isNaN(ctrl.defuserData.amount) || ctrl.defuserData.amount <= 0 || ctrl.defuserData.recipient_email === 'undefine' || ctrl.defuserData.recipient_email === '';
    }
    ctrl.submitForm = function()
    {
        if(typeof ctrl.defuserData._id === 'undefined')
        {
          ctrl.addDefinitions();
        }
        else {
          ctrl.updateDefinition(ctrl.defuserData._id);
        }
    };

    ctrl.deleteDefinition = function(_id)
    {
        $http.delete('/definitions?_id='+_id).then(
            function(rep) {
                ctrl.refreshDefinitions();
                ctrl.message = {
                    ok: 'Definition deleted! '
                };
             },
            function(err) { ctrl.message = 'Something went wrong!'; }
        );
    };

    ctrl.refreshDefinitions();
}]);
