app1.controller("Transfer", [ "$http", "$scope", "$uibModal", "common", function($http, $scope, $uibModal, common) {
    var ctrl = this;
    ctrl.account = {};
    ctrl.transfer = { recipient: '', amount: 1, description: '' };
    ctrl.existDefinitions = {};
    var zeroData = function() {
      console.log('zerodata called');
        ctrl.transfer = { recipient: '', amount: 0, description: '' };
        ctrl.message = {};
    }

    ctrl.doTransfer = function() {
        $http.post('/account', ctrl.transfer).then(
            function(rep) { ctrl.account = rep.data; refreshRecipients(); zeroData(); ctrl.message = { ok: 'Transfer successful' }; },
            function(err) { ctrl.message = { error: 'Transfer failed' }; }
        );
    };

    ctrl.transferInvalid = function() {
        return isNaN(ctrl.transfer.amount) || ctrl.transfer.amount <= 0 || ctrl.account.balance - ctrl.transfer.amount < ctrl.account.limit;
    }

    $http.get('/account').then(
        function(rep) { ctrl.account = rep.data; },
        function(err) { ctrl.account = {}; }
    );

//TO GET from existing definitions and make that work.....
    var getDefinitions = function() {
      $http.get('/definitions').then(
          function(rep) {
             ctrl.existDefinitions = rep.data;
             console.log(ctrl.existDefinitions);
          },
          function(err) { ctrl.message = 'Error retrieving definitions!'; }
      );
    }
    ctrl.selectDef = function(d) {
    console.log("What I get",d);
    if(d===null)
    {
      console.log("condition working")
      zeroData();
    }
    else {
      ctrl.transfer.recipient = d.recipient_email;
      ctrl.transfer.amount = d.amount;
      ctrl.transfer.description = d.description;
    }

    };
//ENDS here......
    var refreshRecipients = function() {
        $http.get('/recipients').then(
            function(rep) { ctrl.recipients = rep.data; },
            function(err) { ctrl.recipients = []; }
        );
        getDefinitions();
    }

    refreshRecipients();

    ctrl.modalOpen = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title-top',
            ariaDescribedBy: 'modal-body-top',
            templateUrl: '/html/modaltransfer.html',
            controller: 'ModalTransfer',
            controllerAs: 'ctrl',
            scope: ctrl.scope,
            resolve: {
                recipients: function () {
                    return ctrl.recipients;
                }
            }
        });
        modalInstance.result.then(
            function(data) {
                console.log(data);
            });
    };

    ctrl.confirm = function() {
        common.confirm("Confirm the operation", function(answer) {
            console.log("Confirmation " + answer);
        });
    };

 }]);
