app1.controller("Home", [ '$http', 'common', function($http, common) {
    var ctrl = this;
    ctrl.session = common.session;
    ctrl.message = {};

    ctrl.creds = { email: 'susmita@gmail.com', password: '1234' };

    ctrl.doLogin = function() {
        $http.post('/login', ctrl.creds).then(
            function(rep) {
                ctrl.session.login = rep.data.email;
                ctrl.session.accountNo = rep.data._id;
                common.rebuildMenu(); ctrl.message = { ok: 'Login successful'  };
            },
            function(err) { ctrl.message = { error: 'Login failed'}; }
        );
    }

    ctrl.doLogout = function() {
        $http.delete('/login').then(
            function(rep) { ctrl.session.login = null; common.rebuildMenu(); ctrl.message = { ok: 'Logout successful'}; }
        );
    }
}]);
